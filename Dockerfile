## TODO: This needs to be hosted on HP's AWS Docker repo:
FROM quay.io/mattbot/base:ubuntu-18.04-4

LABEL maintainer="matt.ridenour@hp.com"

RUN apt-get update \
  && apt-get -y --no-install-recommends install \
    autoconf \
    bison \
    byacc \
    build-essential \
    cron \
    git \
    libffi-dev \
    libgdbm-dev \
    libgdbm5 \
    libncurses5-dev \
    libreadline6-dev \
    libssl-dev \
    libyaml-dev \
    subversion \
    yarn \
    zlib1g-dev \
  && rm -rf /var/lib/apt/lists/*

ENV RUBY_VERSION="2.6.3" RUBY_PREFIX=/usr/local

# Disable default RDoc/ri generation when installing gems
RUN echo 'gem: --no-rdoc --no-ri' >> /usr/local/etc/gemrc \

# Use ruby-build to install Ruby
  && git clone https://github.com/sstephenson/ruby-build.git /tmp/ruby-build \

# Install ruby via ruby-build
  && /tmp/ruby-build/bin/ruby-build -v "$RUBY_VERSION" "$RUBY_PREFIX" \

  && rm -rf /tmp/ruby-build \

 # update rubygems
  && gem update --system \
  && gem install bundler --force \
  && gem install rake --force \
  && gem install concurrent-ruby

CMD ["irb"]

# for dependant images, again update rubygems then install bundler upon building off this image to ensure everything's up-to-date
ONBUILD RUN gem update --system \
  && gem install bundler --force
